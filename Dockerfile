FROM registry.access.redhat.com/ubi8/python-39:1-87.1669838026 as builder
USER root
ENV POETRY_HOME /opt/poetry
ENV PATH "${POETRY_HOME}/bin:${PATH}"
WORKDIR /tmp/build
COPY . .
RUN make depend && poetry build

FROM quay.io/centos/centos:stream8
USER root
WORKDIR /opt/python_template
COPY --from=builder /tmp/build/dist/*.whl .
RUN dnf install -y \
    python39-pip-20.2.4-7.module_el8.6.0+961+ca697fb5 \
    python39-devel-3.9.13-1.module_el8.7.0+1178+0ba51308 \
    make-1:4.2.1-11.el8.x86_64 \
    && dnf clean all
RUN pip3 install --no-cache-dir ./*.whl
