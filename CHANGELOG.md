# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [Unreleased]

### Added

* Poetry
  ([!16](https://gitlab.com/CentOS/automotive/coffre/python_template/-/merge_requests/16)
* Renovate precommit
  ([!6](https://gitlab.com/CentOS/automotive/coffre/python_template/-/merge_requests/6)
* Renovate presets
  ([!3](https://gitlab.com/CentOS/automotive/coffre/python_template/-/merge_requests/3)
